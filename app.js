const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const bodyParser = require('body-parser')
const passport = require('passport')

const indexRouter = require('./routes/index')
const login = require('./routes/login')
const game = require('./routes/game')
const text = require('./routes/text')

const app = express()
const server = require('http').createServer(app)


require('./config/passport.js')
require('./services/socketGameController')(server)

app.use(logger('dev'))
app.use(express.json())
app.use(cookieParser())
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: false }))
app.use(passport.initialize())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/login', login)
app.use('/game', game)
app.use('/text', text)

server.listen(3001)
module.exports = app
