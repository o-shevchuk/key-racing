const express = require('express')
const router = express.Router()
const { Auth } = require('../services/Auth')

router.post('/', function (req, res, next) {
  const user = req.body

  Auth.login(user)
    .then(token => res.status(200).json({ auth: true, token }))
    .catch(err => res.status(401).json({ auth: false, err }))
})

module.exports = router
