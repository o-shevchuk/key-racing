const { waitFor } = require('../helpers/utils')
const MessageFactory = require('./MessageFactory')

class GameAnnouncerBot {
  constructor () {
    this.quickDelay = 5 * 1000
    this.longDelay = 30 * 1000
  }

  async greetings(io, Game){
    // await waitFor(this.quickDelay)
    const message = new MessageFactory('GREETINGS').get()
    io.to(Game.next.room).emit('updateAnnouncer', { message })
  }


  async start (io, Game) {
    const users = Game.next.users
    const room = Game.next.room
    for (const user of users) {
      await waitFor(this.quickDelay)
      const message = new MessageFactory('PRESENT_USER', user).get()
      io.to(room).emit('updateAnnouncer', { message })

    }
  }

  async showRandomMessages (io, Game) {
    const duration = Game.duration
    const room = Game.next.room
    let timeLeft = Game.next.startDate + duration - Date.now()
    let counter = 0

    do {
      await waitFor(this.longDelay)
      io.to(room).emit('updateAnnouncer', { message: `RANDOM MESSAGE PLACEHOLDER ${counter}` })
      timeLeft -= this.longDelay
    } while (timeLeft - this.longDelay > 0)
  }

  async end (io, Game) {
    const users = Game.current.users
    for (const user of users) {
      const message = new MessageFactory('SHOW_RESULTS', user).get()

      await waitFor(this.quickDelay)
      io.to(Game.current.room).emit('updateAnnouncer', message)
    }
  }
}

module.exports = GameAnnouncerBot
