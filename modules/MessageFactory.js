const { getRandomIndexByLength } = require('../helpers/utils')

class Message {
  constructor (text) {
    this.text = text
  }
  get () {
    return this.text
  }
}

class Greetings extends Message {
  constructor () {
    super(`Доброго дня! Пані та Панове, Лееееді і не леді! Сьогодні ми з вами поринемо у неймовірну пригоду гонок, тож представимо наших учасників!`)
  }
}

class AlmostFinished extends Message {
  constructor ({ name }) {
    super(`Гонщик ${name} на фінішній прямій`)
  }
}

class FinishAnnouncement extends Message {
  constructor ({ name, car, finishTime }) {
    super(`Гравець <b>${name}</b> перетнув фінішну пряму на ${car} за ${finishTime / 1000}c`)
  }
}

class PresentUser extends Message {
  constructor ({ name, car }) {
    super(`Сьогодні в гонці приймає участь ${car} з гравцем на борту ${name}`)
  }
}

class ShowResults extends Message {
  constructor ({ name, score, finishTime }) {
    const messages = ['Неймовірно!', 'Вражаюче!', 'Круть!', 'Блискавично! Захоплююче!']
    const index = getRandomIndexByLength(messages)
    super(`${messages[index]} Гонщик ${name} фінішував з результатом ${score} за ${finishTime}`)
  }
}

class MessageFactory {
  constructor (type, user) {
    switch (type) {
      case 'GREETINGS':
        return new Greetings()
      case 'ALMOST_FINISHED':
        return new AlmostFinished(user)
      case 'FINISH_ANNOUNCEMENT':
        return new FinishAnnouncement(user)
      case 'PRESENT_USER':
        return new PresentUser(user)
      case 'SHOW_RESULTS':
        return new ShowResults(user)
    }
  }
}

module.exports = MessageFactory
