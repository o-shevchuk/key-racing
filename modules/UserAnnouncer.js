const MessageFactory = require('./MessageFactory')

class UserAnnouncer {
  constructor (user, Game, io) {
    Object.assign(this, user)

    return new Proxy(this, {
      set (target, name, value) {
        target[name] = value
        switch (name) {
          case 'score':
            target.scoreWatcher(value, Game, io)
            break
          case 'finishTime':
            target.finishWatcher(value, Game, io)
            break
        }

        return true
      }
    })
  }
  scoreWatcher (score, Game, io) {
    const textLength = Game.current.textLength
    if (textLength - score === 50) {
      const message = new MessageFactory('ALMOST_FINISHED', this).get()
      io.to(Game.current.room).emit('updateAnnouncer', { message })
    }
  }

  finishWatcher (value, Game, io) {
    const message = new MessageFactory('FINISH_ANNOUNCEMENT', this).get()
    io.to(Game.current.room).emit('updateAnnouncer', { message })
  }
}


module.exports = UserAnnouncer
