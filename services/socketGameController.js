const GameAnnouncerBot = require('../modules/GameAnnouncerBot')
const Game = require('./Game')
const jwt = require('jsonwebtoken')
const users = require('../db/users.json')
const { findUserByLogin, findUserById } = require('../helpers/utils')
const UserAnnouncer = require('../modules/UserAnnouncer')

module.exports = function (server) {
  const io = require('socket.io')(server)
  const bot = new GameAnnouncerBot(io, Game)

  Game.onJoinHandler(game => {
    io.to(Game.next.room).emit('joined', { game })
    bot.greetings(io, Game)
  })

  Game.onStartHandler(game => {
    bot.start(io, Game)
    bot.showRandomMessages(io, Game)
    io.to(Game.next.room).emit('start', { game, duration: Game.duration })
  })

  Game.onEndHandler((game, winner) => {
    bot.end(io, Game)
    io.to(Game.current.room).emit('endGame', { game, winner })
  })

  io.on('connection', socket => {
    socket.on('join', ({ token }) => {
      const user = jwt.verify(token, 'secret')
      if (user) {
        const { name, car } = users.find(findUserByLogin(user.login))
        const newUser = {
          name,
          isActive: true,
          id: socket.id,
          score: 0,
          finishTime: 0,
          car
        }

        const userWithAnnouncer = new UserAnnouncer(newUser, Game, io)
        const room = Game.join(userWithAnnouncer)

        socket.join(room)
        Game.willStart()
      }
    })

    socket.on('disconnect', () => {
      try {
        console.log('disconnect')
        Game.disconnectUser(socket.id)
      } catch (err) {
        console.log(err)
      }
    })

    socket.on('submit', ({ score }) => {
      try {
        const index = Game.current.users.findIndex(findUserById(socket.id))
        const currentUser = Game.current.users[index]
        currentUser.score = score

        if (score === Game.current.textLength) {
          currentUser.finishTime = Date.now() - Date.parse(Game.current.startDate)
        }

        io.to(Game.current.room).emit('updateScore', { game: Game.current })
      } catch (error) {
        console.log(error)
      }
    })
  })
}

// це має бути схожим на контролер адже тут відбувається ініціалізація івентів гри і сокетів
