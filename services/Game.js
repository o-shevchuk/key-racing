const { delay, findUserById, sortByScore, getRandomIndexByLength } = require('../helpers/utils')
const texts = require('../db/texts.json')
const GameAnnouncerBot = require('./../modules/GameAnnouncerBot')

class Game {
  constructor () {
    this.initGames()
    this.startTimer = null
    this.endTimer = null
    this.delay = 5 * 1000
    this.duration = 60 * 1000
  }

  initGames () {
    this.current = this.generateNewGame()
    this.next = this.generateNewGame()
  }

  generateNewGame () {
    const index = getRandomIndexByLength(texts)
    const template = {
      startDate: '',
      room: 'room' + Math.random(),
      users: [],
      textId: index,
      textLength: texts[index].text.length
    }

    return template
  }

  join (user) {
    this.next.users.push(user)
    return this.next.room
  }

  disconnectUser (id) {
    const currentUser = this.current.users.find(findUserById(id))
    if (currentUser) {
      currentUser.isActive = false
      if (this.current.users.length === 0) {
        this.endHandler(this.current)
      }
    } else {
      const nextUserIndex = this.next.users.findIndex(findUserById(id))
      if (~nextUserIndex) {
        this.removeUserFromNext(nextUserIndex)
      }
    }
  }

  removeUserFromNext (nextUserIndex) {
    this.next.users.splice(nextUserIndex, 1)
    if (this.next.users.length === 0) {
      clearTimeout(this.startTimer)
      this.startTimer = null
      this.next = this.generateNewGame()
    }
  }

  async willStart () {
    if (this.isFirstUserInNext) {
      const startDelay = this.getStartDelay()
      this.setStartDate(startDelay)
      this.joinHandler(this.next)

      await delay(this, 'startTimer', startDelay)
      this.startHandler(this.next)
      this.prepareNext()

      await delay(this, 'endTimer', this.duration)
      this.endHandler(this.current)
    } else {
      this.joinHandler(this.next)
    }
  }

  setStartDate (startDelay) {
    this.next.startDate = new Date(Date.now() + startDelay)
  }

  getStartDelay () {
    return this.isCurrentGameActive
      ? this.delay + (Date.parse(this.current.startDate) + this.duration - Date.now())
      : this.delay
  }

  prepareNext () {
    this.current = this.next
    this.next = this.generateNewGame()
  }

  onStartHandler (handler) {
    this.startHandler = handler
  }

  onJoinHandler (handler) {
    this.joinHandler = handler
  }

  onEndHandler (handler) {
    this.endHandler = game => {
      game.users.sort(sortByScore)
      const winner = game.users[0].name
      handler(game, winner)
    }
  }

  get isFirstUserInNext () {
    return this.next.users.length == 1
  }

  get isCurrentGameActive () {
    return this.current.users.length > 0
  }
}

module.exports = new Game()

//це патерн singleton, адже тут експортовано інстанс гри, де б цей модуль не зарекваєрили - отримаємо той самий інстанс.