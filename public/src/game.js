import './styles/game.scss'
import { isAuthentified, redirectToHomePage } from './api/auth'
import GameView from './components/GameView'

const startApp = () => {
  if (!isAuthentified()) {
    redirectToHomePage()
  } else {
    const gameApp = new GameView('#app');
  }
}

window.onload = startApp
