import View from './view'

export default class Announcer extends View {
  constructor () {
    super()

    this.init()
  }

  init () {
    const portrait = this.createElement({
      tagName: 'img',
      className: 'portrait',
      attributes: { src: './media/images/announcer.png' }
    })
    this.comment = this.createElement({ tagName: 'span', className: 'comment' })
    this.element = this.createElement({ className: 'announcer' })
    this.element.append(portrait, this.comment)
    this.hide()
  }

  showComment (comment) {
    if (comment){
      this.comment.innerHTML = `${comment}`
    }
  }
}
