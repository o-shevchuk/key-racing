import View from './view'
import moment from 'moment'

export default class Timer extends View {
  constructor () {
    super()
    this.init()
  }

  init () {
    this.element = this.createElement({ className: 'timer' })
    this.hide()
  }

  start (dateToStopTimer, customMessage) {
    this.show()
    const interval = 1000

    const timer = setInterval(() => {
      let timeLeft = dateToStopTimer - Date.now()
      if (timeLeft > 0) {
        const splittedTime = moment(timeLeft)
          .format('m s')
          .split(' ')
        const formatedTime = `${splittedTime[0]}m ${splittedTime[1]}s`

        this.element.innerHTML = customMessage ? customMessage(formatedTime) : `${formatedTime}<br> until the end`
      } else {
        this.element.innerHTML = `no time left`
        clearInterval(timer)
      }
    }, interval)
  }
}
