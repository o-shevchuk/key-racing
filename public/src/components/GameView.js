import { getText } from './../api/auth'
import io from 'socket.io-client'
import KeyRace from './KeyRace'
import View from './view'
import Info from './Info'
import PlayersView from './PlayersView'
import Timer from './Timer'
import Button from './Button'
import Announcer from './Announcer'

export default class GameView extends View {
  constructor (root) {
    super()
    this.init(root)
    const socket = io('http://localhost:3001')
    this.initSocketHandlers(socket)
  }

  init (root) {
    this.element = this.createElement({ className: 'game-view' })

    this.info = new Info()
    this.keyRace = new KeyRace()
    this.playersView = new PlayersView()
    this.endTimer = new Timer()
    this.startTimer = new Timer()
    this.button = new Button()
    this.announcer = new Announcer()

    this.element.append(
      this.playersView.element,
      this.info.element,
      this.startTimer.element,
      this.keyRace.element,
      this.endTimer.element,
      this.button.element,
      this.announcer.element
    )

    document.querySelector(root).appendChild(this.element)
  }

  initSocketHandlers (socket) {
    const jwt = localStorage.getItem('jwt')

    socket.emit('join', {
      token: jwt
    })

    socket.on('joined', async ({ game: { startDate, textId, room } }) => {
      console.log(room)
      this.info.showMessage('The game will start in :')
      this.startTimer.start(Date.parse(startDate), date => date)

      const response = await getText(textId)
      const { text } = await response.json()

      this.keyRace.initModel(text)

      socket.on('start', ({ game: { users, startDate }, duration }) => {
        this.info.hide()
        this.startTimer.hide()
        this.keyRace.start()
        this.announcer.show()
        this.playersView.render(users)
        this.endTimer.start(Date.parse(startDate) + duration)
      })
    })

    this.keyRace.onSubmit(() => {
      socket.emit('submit', {
        score: this.keyRace.entered.length,
        token: jwt
      })
    })

    socket.on('updateScore', ({ game: { users } }) => {
      this.playersView.render(users)
    })

    socket.on('updateAnnouncer', ({ message }) => {
      this.announcer.showComment(message)
    })

    socket.on('endGame', ({ winner }) => {
      this.keyRace.hide()
      this.endTimer.hide()
      this.keyRace.isStarted = false
      this.info.showWinnerMessage(winner)
      this.button.show()
      // socket.disconnect()
    })

    this.button.onClick(() => {
      location.reload()
    })

  }
}
