import View from './view'

class Info extends View {
  constructor () {
    super()
    this.init()
  }
  init () {
    this.element = this.createElement({ className: 'info-msg' })
  }

  showMessage (message) {
    this.show()
    this.element.innerHTML = `${message}`
  }
  showWinnerMessage(name){
    this.show()
    this.element.innerHTML = `Player ${name} won the race`
  }


}

export default Info
