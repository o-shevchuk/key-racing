const delay = (ctx, prop, time) =>
  new Promise(
    resolve =>
      (ctx[prop] = setTimeout(() => {
        ctx[prop] = null
        resolve()
      }, time))
  )

const waitFor = time => new Promise(resolve => setTimeout(resolve, time))

const findUserById = id => user => user.id == id //curry function

const findUserByLogin = login => user => user.login == login

const sortByScore = (a, b) => a.score - b.score

const getRandomIndexByLength = obj => Math.round(Math.random() * (obj.length - 1))

module.exports = {
  delay,
  findUserById,
  findUserByLogin,
  sortByScore,
  waitFor,
  getRandomIndexByLength
}

//цей модуль з допоміжними функціями створено у вигляді фасаду